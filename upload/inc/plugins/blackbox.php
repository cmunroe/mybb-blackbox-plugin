<?php


// Prevent someone directly connecting to this.
if(!defined('IN_MYBB')) {
    die();
}

// Detail our information
function blackbox_info() {

    return array(
		"name"			=> "Blackbox",
		"description"	=> 'Block Registration from known hosting, VPN, and Proxy IPs.',
		"website"		=> "https://gitlab.com/cmunroe/mybb-blackbox-plugin",
		"author"		=> "Cameron Munroe",
		"authorsite"	=> "https://gitlab.com/cmunroe",
		"version"		=> "0.2",
		'compatibility'	=> '18*',
		'codename'		=> 'blackbox'
	);

}

function blackbox_activate(){
    global $db;

    $blackbox_group = array(
        'name' => "blackbox",
        'title' => "Blackbox",
        'description' => "Blackbox Settings",
        "disporder" => '2',
        'isdefault' => "0"
    );

    $db->insert_query('settinggroups', $blackbox_group);
    $gid = $db->insert_id();

    $pluginSettings[] = array(
        
        'name' => "blackbox_enabled",
        'title' => "Enabled",
        'description' => "Do you want to enable Blackbox registration screening?",
        'optionscode' => 'yesno',
        'value' => "0",
        'disporder' => '1',
        'gid' => intval($gid)

    );

    $pluginSettings[] = array(
        
        'name' => "blackbox_key",
        'title' => "RapidAPI Key",
        'description' => "This is the RapidAPI Key that will authenticate your requests.",
        'optionscode' => 'text',
        'value' => "rapidapi",
        'disporder' => '2',
        'gid' => intval($gid)

    );

    // Detections:
    // bogon
    // cloud
    // hosting
    // proxy
    // spamhaus
    // tor

    $pluginSettings[] = array(
        
        'name' => "blackbox_bogon",
        'title' => "Block: Bogons",
        'description' => "Do you want to block bogons from registering?",
        'optionscode' => 'yesno',
        'value' => "0",
        'disporder' => '3',
        'gid' => intval($gid)

    );

    $pluginSettings[] = array(
        
        'name' => "blackbox_cloud",
        'title' => "Block: Cloud",
        'description' => "Do you want to block known cloud providers from registering?",
        'optionscode' => 'yesno',
        'value' => "0",
        'disporder' => '4',
        'gid' => intval($gid)

    );

    $pluginSettings[] = array(
        
        'name' => "blackbox_hosting",
        'title' => "Block: Hosting",
        'description' => "Do you want to block known hosting providers from registering?",
        'optionscode' => 'yesno',
        'value' => "0",
        'disporder' => '5',
        'gid' => intval($gid)

    );

    $pluginSettings[] = array(
        
        'name' => "blackbox_proxy",
        'title' => "Block: Proxy",
        'description' => "Do you want to block known proxy servers from registering?",
        'optionscode' => 'yesno',
        'value' => "1",
        'disporder' => '6',
        'gid' => intval($gid)

    );

    $pluginSettings[] = array(
        
        'name' => "blackbox_spamhaus",
        'title' => "Block: Bad Networks",
        'description' => "Do you want to block known bad networks from registering?",
        'optionscode' => 'yesno',
        'value' => "0",
        'disporder' => '7',
        'gid' => intval($gid)

    );

    $pluginSettings[] = array(
        
        'name' => "blackbox_tor",
        'title' => "Block: Tor",
        'description' => "Do you want to block known Tor servers from registering?",
        'optionscode' => 'yesno',
        'value' => "1",
        'disporder' => '8',
        'gid' => intval($gid)

    );

    $pluginSettings[] = array(
        
        'name' => "blackbox_message",
        'title' => "Message",
        'description' => "What do you want to tell the users that got blocked?",
        'optionscode' => 'text',
        'value' => "You are using a VPN / Proxy / Tor, which is not allowed.",
        'disporder' => '8',
        'gid' => intval($gid)

    );


    foreach($pluginSettings as $setting)
    {
        $db->insert_query("settings", $setting);
    }
    
    rebuild_settings();

}

function blackbox_deactivate(){

    global $db;

	$query = $db->write_query("SELECT gid FROM ".TABLE_PREFIX."settinggroups WHERE name='blackbox'");

    $g = $db->fetch_array($query);

    $db->write_query("DELETE FROM ".TABLE_PREFIX."settinggroups WHERE gid='".$g['gid']."'");
    $db->write_query("DELETE FROM ".TABLE_PREFIX."settings WHERE gid='".$g['gid']."'");

    rebuild_settings();
    
}


// Looks for registrations, and runs check function.
$plugins->add_hook("member_register_start", "blackbox_do");

// Main Function for Checking Registrations.
function blackbox_do(){
    
    global $session, $lang, $db, $mybb;

    // Plugins is enabled or not?
    if($mybb->settings['blackbox_enabled'] == 1){

        $ip = $session->ipaddress;

        $url = "https://blackbox.p.rapidapi.com/v2/obj/" . $ip;
        
        $options = array(
            'http'=>array(
              'method'=>"GET",
              'header'=>"x-rapidapi-host: blackbox.p.rapidapi.com\r\n" .
                        "x-rapidapi-key: " . $mybb->settings['blackbox_key'] . "\r\n"
            )
        );
        $context = stream_context_create($options);

        $res = file_get_contents($url, false, $context);

        if(!strpos($http_response_header[0], "200")){

            return;

            // TODO: 
            // Probably should add logging of failed requests.

        }

        $res = json_decode($res);

        if($res->error  !== null){

            return;
            
            // TODO: 
            // Probably should add logging of failed requests.

        }
        
        if($mybb->settings['blackbox_bogon'] == 1 && $res->detection->bogon === true){

            // We caught them Will Robinson.

            error($mybb->settings['blackbox_message']);
            exit();

        }

        if($mybb->settings['blackbox_hosting'] == 1 && $res->detection->hosting === true){

            // We caught them Will Robinson.

            error($mybb->settings['blackbox_message']);
            exit();

        }    

        if($mybb->settings['blackbox_proxy'] == 1 && $res->detection->proxy === true){

            // We caught them Will Robinson.

            error($mybb->settings['blackbox_message']);
            exit();

        }
        
        
        if($mybb->settings['blackbox_spamhaus'] == 1 && $res->detection->spamhaus === true){

            // We caught them Will Robinson.

            error($mybb->settings['blackbox_message']);
            exit();

        }

                
        if($mybb->settings['blackbox_tor'] == 1 && $res->detection->tor === true){

            // We caught them Will Robinson.

            error($mybb->settings['blackbox_message']);
            exit();

        }


        // This person looks clean to us brother.
        return;

    }

}
